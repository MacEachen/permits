from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

CHOICES = [
    ("Feature","Feature"),
    ("TV","TV"),
    ("Movie","Movie"),
    ("Commercial","Commercial"),
    ("Music Video","Music Video"),
    ("Student Film","Student Film"),
    ("Live Performance","Live Performance"),
    ("Other","Other"),
]

class ContactForm(forms.Form):
    first_name = forms.CharField()
    last_name = forms.CharField()    
    company = forms.CharField()
    email = forms.EmailField()
    phone = forms.CharField()
    message = forms.CharField(widget=forms.Textarea)
    choices = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)

    def clean_first_name(self):
        data = self.cleaned_data['first_name']
        if not data:
            raise ValidationError(_('Please complete this field.'))
        return data

    def clean_last_name(self):
        data = self.cleaned_data['last_name']
        if not data:
            raise ValidationError(_('Please complete this field.'))
        return data

    def clean_company(self):
        data = self.cleaned_data['company']
        #if not data:
        #    raise ValidationError(_('Please complete this field.'))
        return data

    def clean_email(self):
        data = self.cleaned_data['email']
        if not data:
            raise ValidationError(_('Please complete this field.'))
        return data

    def clean_phone(self):
        data = self.cleaned_data['phone']
        if not data:
            raise ValidationError(_('Please complete this field.'))
        return data

    def clean_message(self):
        data =self.cleaned_data['message']
        if not data:
            raise ValidationError(_('Please enter your message here.'))
        return data

    def clean_choices(self):
        data = self.cleaned_data['choices']
        if not data:
            raise ValidationError(_('Please select a choice.'))
        return data
