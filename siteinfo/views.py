from django.shortcuts import redirect
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from .models import ASection, Alpha, BSection, Bravo, Landing, Contact
from .forms import ContactForm

class MainView(FormView):
    template_name = 'siteinfo/main.html'
    form_class = ContactForm
    contact = Contact.objects.first()

    def get_context_data(self,*args,**kwargs):
        context = super().get_context_data(*args,**kwargs)
        context['landing'] = Landing.objects.first()
        context['alpha'] = ASection.objects.first()
        context['bravo'] = BSection.objects.first()
        context['contact'] = self.contact

        return context

    def form_valid(self,form):
        args = [
            ('first_name',form.cleaned_data['first_name']),
            ('last_name', form.cleaned_data['last_name']),
            ('company', form.cleaned_data['company']),
            ('email', form.cleaned_data['email']),
            ('phone', form.cleaned_data['phone']),
            ('choice', form.cleaned_data['choices']),
            ('message', form.cleaned_data['message']),
        ]
        

        self.contact.send_message(args)
        return redirect('/thank-you/')
        

class ThankYouView(TemplateView):
    template_name = 'siteinfo/thankyou.html'
    contact = Contact.objects.first()
    
    def get_context_data(self,*args,**kwargs):
        context = super().get_context_data(*args,**kwargs)
        context['landing'] = Landing.objects.first()
        context['alpha'] = ASection.objects.first()
        context['bravo'] = BSection.objects.first()
        context['contact'] = self.contact

        return context
    
class DetailView(TemplateView):
    template_name  = 'siteinfo/detail.html'
    contact = Contact.objects.first()
    
    def get_context_data(self, *args, **kwargs):
        slug = kwargs.get('slug')
        context = super().get_context_data(*args, **kwargs)
        context['landing'] = Landing.objects.first()
        context['alpha'] = ASection.objects.first()
        context['bravo'] = BSection.objects.first()
        context['contact'] = self.contact
        context['obj'] = Alpha.objects.get(slug=slug)
        return context
