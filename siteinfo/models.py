from django.db import models
from django.template.defaultfilters import slugify
from django.core.exceptions import ValidationError
from django.core import mail
from django.core.mail.backends.smtp import EmailBackend
from django.core.mail import EmailMessage
from django.core.mail import send_mail

class ASection(models.Model):
    title = models.CharField(max_length=250)
    summary = models.TextField()

    def save(self, *args, **kwargs):
        if not self.pk and ASection.objects.exists():
            raise ValidationError('There can only be one')
        return super().save(*args,**kwargs)

        
    


class Alpha(models.Model):
    section = models.ForeignKey(ASection, on_delete=models.CASCADE)    
    title = models.CharField(max_length=500)
    summary = models.TextField()
    text = models.TextField()
    button_prompt = models.CharField(max_length=250)    
    order = models.IntegerField()

    slug = models.SlugField(blank=True,null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args,**kwargs)

    class Meta:
        ordering = ['order']


class BSection(models.Model):
    title = models.CharField(max_length=250)
    summary = models.TextField()

    def save(self, *args, **kwargs):
        if not self.pk and BSection.objects.exists():
            raise ValidationError('There can only be one')
        return super().save(*args,**kwargs)
    
    
    
class Bravo(models.Model):
    section = models.ForeignKey(BSection, on_delete=models.CASCADE)
    title = models.CharField(max_length=500)
    summary = models.TextField()
    order = models.IntegerField()

    class Meta:
        ordering = ['order']

        
class Landing(models.Model):
    title =  models.CharField(max_length=250)
    text = models.TextField()
    message = models.TextField()
    button_prompt = models.CharField(max_length=250)

    def save(self, *args, **kwargs):
        if not self.pk and Landing.objects.exists():
            raise ValidationError('There can only be one')
        return super(Landing, self).save(*args,**kwargs)


class Contact(models.Model):
    title = models.CharField(max_length=250)
    summary = models.TextField()
    address = models.CharField(max_length=250)
    city = models.CharField(max_length=100)
    state =  models.CharField(max_length=100)
    zipcode = models.CharField(max_length=5)

    email = models.EmailField()
    phone = models.CharField(max_length=10)

    def format_address(self,*args,**kwargs):
        f = self.address + ',\n' + self.city + ', ' + self.state + ' ' + self.zipcode
        return f

    def format_phone(self,*args,**kwargs):
        p = self.phone

        f = '('+p[:3]+') ' + p[3:6] + ' ' + p[6:]
        return f

    def save(self, *args, **kwargs):
        if not self.pk and Contact.objects.exists():
            raise ValidationError('There can only be one')
        return super(Contact, self).save(*args,**kwargs)
    
    def send_message(self,args):
        subject = 'New Inquiry on AcePermits'

        content = ""
        for (k,v) in args:
            content = content + k + ": " + v + "\n\n"

        with mail.get_connection() as connection:
            msg = EmailMessage(subject,
                               content,
                               self.email,
                               [self.email,],
                               connection=connection
            )
            msg.send()
