from django.contrib import admin
from .models import Landing, ASection, Alpha, BSection, Bravo, Contact
# Register your models here.

class LandingAdmin(admin.ModelAdmin):
    pass


class AlphaInline(admin.StackedInline):
    model = Alpha
    
class AlphaAdmin(admin.ModelAdmin):
    inlines = [AlphaInline,]
    #fields = ['order','title','summary','text'] 
    
class BravoInline(admin.StackedInline):
    model = Bravo
    
class BravoAdmin(admin.ModelAdmin):
    inlines = [BravoInline,]

    
class ContactAdmin(admin.ModelAdmin):
    pass


admin.site.register(Landing, LandingAdmin)
admin.site.register(ASection, AlphaAdmin)
admin.site.register(BSection, BravoAdmin)
admin.site.register(Contact, ContactAdmin)
