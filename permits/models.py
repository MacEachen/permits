from django.db import models

class Service(models.Model):
    title = models.CharField(max_length=200)
    detail = models.TextField()

class Description(models.Model):
    detail = models.TextField()

    def save(self, *args, **kwargs):
        if not self.pk and Description.objects.exists():
            raise ValidationError('There can only be one')
        return super(Description, self).save(*args,**kwargs)
